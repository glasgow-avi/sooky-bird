package com.catch22.sookybird.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.catch22.sookybird.SookyBird;

public class DesktopLauncher
{
	private static final int width = 400, height = 800;
	private static final String title = "Sooky Bird";

	public static void main (String[] arg)
	{
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = width;
		config.height = height;
		config.title = title;

		new LwjglApplication(new SookyBird(), config);
	}
}
