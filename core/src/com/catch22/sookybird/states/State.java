package com.catch22.sookybird.states;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

public abstract class State
{
    protected OrthographicCamera camera;
    protected Vector3 mouse;
    protected GameStateManager gameStateManager;
    public void dispose()
    {
        background.dispose();
    }

    public static Texture background;

    public State(GameStateManager gameStateManager)
    {
        background = new Texture("background.png");

        this.gameStateManager = gameStateManager;
        camera = new OrthographicCamera();
        mouse = new Vector3();
    }

    public abstract void handleInput();
    public abstract void update(float deltaT);
    public abstract void render(SpriteBatch spriteBatch);
}
