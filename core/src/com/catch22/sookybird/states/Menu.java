package com.catch22.sookybird.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.catch22.sookybird.SookyBird;

public class Menu extends State
{
    private Texture playButton, titleText;

    public Menu(GameStateManager gameStateManager)
    {
        super(gameStateManager);

        camera.setToOrtho(false, SookyBird.width / 2, SookyBird.height / 2);

        titleText = new Texture("gametitle.png");
        playButton = new Texture("playbutton.png");
    }

    @Override
    public void handleInput()
    {
        if(Gdx.input.justTouched())
        {
            gameStateManager.pop();
            gameStateManager.push(new Play(gameStateManager));
        }
    }

    @Override
    public void update(float deltaT)
    {
        handleInput();
    }

    @Override
    public void render(SpriteBatch spriteBatch)
    {
        spriteBatch.setProjectionMatrix(camera.combined);
        spriteBatch.begin();

        spriteBatch.draw(background, 0, 0, SookyBird.width, SookyBird.height);
        spriteBatch.draw(titleText, camera.position.x - 2 * playButton.getWidth() / 2, 2 * camera.position.y - 4 * titleText.getHeight() / 2, 2 * playButton.getWidth(), titleText.getHeight());
        spriteBatch.draw(playButton, camera.position.x - playButton.getWidth() / 2, camera.position.y - playButton.getHeight() / 2);

        spriteBatch.end();
    }

    @Override
    public void dispose()
    {
        playButton.dispose();
        titleText.dispose();
    }
}