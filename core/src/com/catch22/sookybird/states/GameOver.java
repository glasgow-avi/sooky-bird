package com.catch22.sookybird.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.catch22.sookybird.SookyBird;
import com.catch22.sookybird.utilities.ScoreKeeper;

public class GameOver extends State
{
    private Texture gameover;

    public GameOver(GameStateManager gameStateManager)
    {
        super(gameStateManager);

        camera.setToOrtho(false, SookyBird.width / 2, SookyBird.height / 2);

        gameover = new Texture("gameover.png");
    }

    @Override
    public void handleInput()
    {
        if(Gdx.input.justTouched() || Gdx.input.isKeyPressed(Input.Keys.SPACE))
        {
            gameStateManager.pop();
            gameStateManager.push(new Play(gameStateManager));
        }
    }

    @Override
    public void update(float deltaT)
    {
        handleInput();
    }

    @Override
    public void render(SpriteBatch spriteBatch)
    {
        spriteBatch.setProjectionMatrix(camera.combined);
        spriteBatch.begin();

        spriteBatch.draw(background, 0, 0, SookyBird.width, SookyBird.height);
        spriteBatch.draw(gameover, camera.position.x - gameover.getWidth() / 2, camera.position.y - gameover.getHeight() / 2);
        ScoreKeeper.render(spriteBatch, camera);

        spriteBatch.end();
    }

    @Override
    public void dispose()
    {
        gameover.dispose();
    }
}