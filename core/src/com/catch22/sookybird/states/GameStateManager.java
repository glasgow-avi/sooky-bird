package com.catch22.sookybird.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Stack;

public class GameStateManager extends Stack<State>
{
    public void update(float deltaT)
    {
        peek().update(deltaT);
    }

    public void render(SpriteBatch spriteBatch)
    {
        peek().render(spriteBatch);
    }
}
