package com.catch22.sookybird.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.catch22.sookybird.SookyBird;
import com.catch22.sookybird.utilities.ScoreKeeper;
import com.catch22.sookybird.utilities.sound.Player;
import com.catch22.sookybird.sprites.Pipe;
import com.catch22.sookybird.sprites.Sookitty;

public class Play extends State
{
    private Sookitty sukriti;
    private Texture ground;
    public static final int groundHeight = SookyBird.height / 8;

    private static final int pipeSpacing = 125;
    private static final int numberOfPipes = 10;
    private Array<Pipe> pipes;

    private boolean started;

    @Override
    public void dispose()
    {
        ground.dispose();
        super.dispose();
        sukriti.dispose();
    }

    public Play(GameStateManager gameStateManager)
    {
        super(gameStateManager);
        sukriti = new Sookitty();

        ground = new Texture("ground.png");

        started = false;

        pipes = new Array<Pipe>();
        for(int i = 0; i < numberOfPipes; i++)
            pipes.add(new Pipe(i * (pipeSpacing + Pipe.width)));

        SookyBird.score = 0;

        camera.setToOrtho(false, SookyBird.width / 2, SookyBird.height / 2);
    }

    @Override
    public void handleInput()
    {
        if(Gdx.input.justTouched() || Gdx.input.isKeyJustPressed(Input.Keys.SPACE))
        {
            started = true;
            sukriti.velocity.y = 250;
        }
    }

    @Override
    public void update(float deltaT)
    {
        handleInput();

        if(started)
            sukriti.update(deltaT, started);

        camera.position.x = sukriti.position.x + 80;

        for(Pipe pipe : pipes)
        {
            if(camera.position.x - camera.viewportWidth / 2 > pipe.topPos.x + Pipe.width)
                pipe.reposition(pipe.topPos.x + (Pipe.width + pipeSpacing) * numberOfPipes);

            if(!started)
                continue;

            boolean hitPipe = pipe.collides(sukriti.bounds);
            boolean hitGround = /*sukriti.bounds.overlaps(new Rectangle(camera.position.x - camera.viewportWidth / 2, 0, SookyBird.width, groundHeight))*/
                    sukriti.position.y + sukriti.bounds.getHeight() < groundHeight;;
            boolean hitSky = sukriti.position.y > SookyBird.height / 2 + sukriti.bounds.getHeight();
            if(hitPipe || hitSky)
                Sookitty.kill(gameStateManager);
            if(hitGround)
            {
                sukriti.position.y -= 2000;
                Sookitty.kill(gameStateManager);
            }

            if(pipe.passedPipe(sukriti.bounds))
            {
                Gdx.app.log("Score", String.valueOf(SookyBird.score));
                SookyBird.score++;
                if(SookyBird.playSoundOnScoreIncrement && !SookyBird.mute)
                    Player.play();
            }
        }

        camera.update();
    }

    @Override
    public void render(SpriteBatch spriteBatch)
    {
        spriteBatch.setProjectionMatrix(camera.combined);

        spriteBatch.begin();

        spriteBatch.draw(background, camera.position.x - camera.viewportWidth / 2, 0, SookyBird.width, SookyBird.height);
        for(Pipe pipe : pipes)
        {
            if(SookyBird.score != 0 || pipe.topPos.x >= sukriti.bounds.x)
            {
                spriteBatch.draw(pipe.top, pipe.topPos.x, pipe.topPos.y);
                spriteBatch.draw(pipe.bottom, pipe.bottomPos.x, pipe.bottomPos.y);
            }
        }

        spriteBatch.draw(ground, camera.position.x - camera.viewportWidth / 2, 0, SookyBird.width, groundHeight);

        sukriti.render(spriteBatch);
        ScoreKeeper.render(spriteBatch, camera);

        spriteBatch.end();
    }
}