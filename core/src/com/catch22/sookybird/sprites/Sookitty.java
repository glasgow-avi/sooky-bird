package com.catch22.sookybird.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.catch22.sookybird.SookyBird;
import com.catch22.sookybird.utilities.sound.Player;
import com.catch22.sookybird.states.GameOver;
import com.catch22.sookybird.states.GameStateManager;

public class Sookitty
{
    private static final int gravity = -15, step = 100;

    public Vector3 velocity, position;
    public Rectangle bounds;

    public Texture sukriti;

    public void dispose()
    {
        sukriti.dispose();
    }

    public static void kill(GameStateManager gameStateManager)
    {
        if(SookyBird.playSoundOnCrash && !SookyBird.mute)
            Player.play();

        try
        {
            Thread.sleep(100);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        gameStateManager.pop();
        gameStateManager.push(new GameOver(gameStateManager));
    }

    public void render(SpriteBatch spriteBatch)
    {
        spriteBatch.draw(sukriti, position.x, position.y, sukriti.getWidth(), sukriti.getHeight());
    }

    public Sookitty()
    {
        sukriti = new Texture("sukriti.png");

        position = new Vector3(SookyBird.width / 4 - sukriti.getWidth() * 3, SookyBird.height / 4 - sukriti.getHeight() / 2, 0);
        velocity = new Vector3(0, 0, 0);

        bounds = new Rectangle(position.x, position.y, sukriti.getWidth(), sukriti.getHeight());
    }

    public void update(float deltaT, boolean started)
    {
        velocity.add(0, gravity, 0);
        velocity.scl(deltaT);

        if(started)
            position.add(0, velocity.y, 0);
        position.add(step * deltaT, 0, 0);

        velocity.scl(1/deltaT);

        bounds.setPosition(position.x, position.y);
    }
}