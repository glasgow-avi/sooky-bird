package com.catch22.sookybird.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.catch22.sookybird.SookyBird;

import java.util.Random;

public class Pipe
{
    public Texture top, bottom;

    private static final int fluctuation = 130, gap = 100, lowest = 200;

    public Vector2 topPos, bottomPos;
    public static int width;
    private Rectangle topBounds, bottomBounds, endOfPipe;

    private static Random random;

    public Pipe(float x)
    {
        top = new Texture("toptube.png");
        bottom = new Texture("bottomtube.png");

        width = bottom.getWidth();

        random = new Random();

        topPos = new Vector2(x, random.nextInt(fluctuation) + gap + lowest);
        bottomPos = new Vector2(x, topPos.y - gap - bottom.getHeight());

        topBounds = new Rectangle(topPos.x, topPos.y, width, top.getHeight());
        bottomBounds = new Rectangle(bottomPos.x, bottomPos.y, width, bottom.getHeight());
        endOfPipe = new Rectangle(bottomPos.x + width / 2, 0, 1, SookyBird.height);
    }

    public void reposition(float x)
    {
        topPos.set(x, random.nextInt(fluctuation) + gap + lowest);
        bottomPos.set(x, topPos.y - gap - bottom.getHeight());

        topBounds.setPosition(topPos.x, topPos.y);
        bottomBounds.setPosition(bottomPos.x, bottomPos.y);
        endOfPipe.setPosition(bottomPos.x + width / 2, 0);
    }

    public boolean passedPipe(Rectangle sukriti)
    {
        if(endOfPipe.overlaps(sukriti))
        {
            endOfPipe.setPosition(endOfPipe.x - 100, endOfPipe.y + 100);
            return true;
        }

        return false;
    }

    public boolean collides(Rectangle sukriti)
    {
        return bottomBounds.overlaps(sukriti) || topBounds.overlaps(sukriti);
    }
}
