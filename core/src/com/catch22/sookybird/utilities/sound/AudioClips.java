package com.catch22.sookybird.utilities.sound;

public class AudioClips
{
    public static final int
    ALSO_THATS_SO_ANNOYING_AVI = 0,
    STOP = 1,
    SERIOUSLY = 2,
    AVI_STOP_IT = 3,
    NO = 4,
    NOT_FUNNY_AVI = 5,
    EWW = 6,
    THIS_IS_NOT_FUNNY = 7,
    ADHIRAJ = 8,
    DID_YOU_RECORD_THAT_AS_WELL = 9,
    AVI_STOP_IT$ = 10,
    PAUSE_THAT_THING = 11,
    PAUSE_IT_NOW = 12;
}
