package com.catch22.sookybird.utilities.sound;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

import java.util.Random;

public class Player
{
    private static final int numberOfClips = 13;
    private static Sound[] clips = new Sound[numberOfClips];
    private static Random random;

    public Player()
    {
        random = new Random();

        for(int clipIndex = 0; clipIndex < clips.length; clipIndex++)
            clips[clipIndex] = Gdx.audio.newSound(Gdx.files.internal(clipIndex + ".wav"));
    }

    public static void play()
    {
        try
        {
            clips[random.nextInt(numberOfClips)].play();
        } catch(Throwable uninitialized)
        {
            new Player();
            clips[random.nextInt(numberOfClips)].play();
        }
    }

    public static void play(int clipIndex)
    {
        try
        {
            clips[clipIndex].play();
        } catch(Throwable uninitialized)
        {
            new Player();
            clips[clipIndex].play();
        }
    }
}