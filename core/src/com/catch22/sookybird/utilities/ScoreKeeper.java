package com.catch22.sookybird.utilities;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.catch22.sookybird.SookyBird;

public class ScoreKeeper
{
    public static Texture[] digits = new Texture[10];

    public ScoreKeeper()
    {
        for(int digit = 0; digit < 10; digit++)
            digits[digit] = new Texture(digit + ".png");
    }

    private static Texture getDigit(int pos)
    {
        return digits[((int) ((SookyBird.score / Math.pow(10, pos)) % 10))];
    }

    public static void render(SpriteBatch spriteBatch, OrthographicCamera camera)
    {
        int size = (int)(camera.viewportWidth / 10);
        try
        {
            spriteBatch.draw(getDigit(2), camera.position.x - size - size / 2, size, size, size);
        } catch(Throwable uninitialized)
        {
            new ScoreKeeper();
            spriteBatch.draw(getDigit(2), camera.position.x - size - size / 2, size, size, size);
        }
        finally
        {
            spriteBatch.draw(getDigit(1), camera.position.x - size / 2, size, size, size);
            spriteBatch.draw(getDigit(0), camera.position.x + size - size / 2, size, size, size);
        }
    }
}
