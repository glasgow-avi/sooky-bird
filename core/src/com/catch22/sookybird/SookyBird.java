package com.catch22.sookybird;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.catch22.sookybird.states.GameStateManager;
import com.catch22.sookybird.states.Menu;

public class SookyBird extends ApplicationAdapter
{
	private GameStateManager gameStateManager;
	private SpriteBatch batch;

	public static int score = 0;

	public static int width, height;

	public static boolean playSoundOnScoreIncrement = true, playSoundOnCrash = false, mute = !playSoundOnCrash && !playSoundOnScoreIncrement;

	@Override
	public void create ()
	{
		gameStateManager = new GameStateManager();
		batch = new SpriteBatch();

		width = Gdx.graphics.getWidth();
		height = Gdx.graphics.getHeight();

		Gdx.gl.glClearColor(0, 1, 0, 1);
		gameStateManager.push(new Menu(gameStateManager));
	}

	@Override
	public void render ()
	{
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		gameStateManager.update(Gdx.graphics.getDeltaTime());
		gameStateManager.render(batch);
	}
}